sortcmd=./build/sorter/sorter

mkdir -p ./in
mkdir -p ./out

echo "___________________________________________________________________________________________Generating testing data"
eval ./build/generator/generator ./in
echo
echo "___________________________________________________________________________________________Do bigsawtoothwaves.bin"
eval $sortcmd ./in/bigsawtoothwaves.bin ./out/bigsawtoothwaves.bin
echo
echo "___________________________________________________________________________________________Do ordered.bin"
eval $sortcmd ./in/ordered.bin ./out/ordered.bin
echo
echo "___________________________________________________________________________________________Do reversedbigsawtoothwaves.bin"
eval $sortcmd ./in/reversedbigsawtoothwaves.bin ./out/reversedbigsawtoothwaves.bin
echo
echo "___________________________________________________________________________________________Do reversedorder.bin"
eval $sortcmd ./in/reversedorder.bin ./out/reversedorder.bin
echo
echo "___________________________________________________________________________________________Do reversedsmallsawtoothwaves.bin"
eval $sortcmd ./in/reversedsmallsawtoothwaves.bin ./out/reversedsmallsawtoothwaves.bin
echo
echo "___________________________________________________________________________________________Do smallsawtoothwaves.bin"
eval $sortcmd ./in/smallsawtoothwaves.bin ./out/smallsawtoothwaves.bin
echo
echo "___________________________________________________________________________________________Checking testing data"
eval ./build/checker/checker ./out


