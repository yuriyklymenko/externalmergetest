# Task

Implement sorting of huge files in some limited scope of RAM

# Implementations details 

The task itself was recognized as a classic external sorting task so it was implemented in the following way.
Do sorting with two phases:
- Split file with parts, sort and save them on disk
- Combine and save all saved parts with merging sorting algorithm

First part I've implemented with Heap sorting as it does not need extra memory and has O(n log(n) complexity.
The second part I did with K-Way merging (or N-Way as I named it in code) with a tree structure. 
Each node of the tree combines and merge N following nodes asynchronously. 
The node which saves the output file is in the root of the tree.   

# Project structure
generator - tool to generate testing data

sorter - requested tool

checker - tool to check if sorted files correspond to generated data

# Compilation & run
./build.sh
./run_all.sh

