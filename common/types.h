#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>
#include <experimental/filesystem>

namespace common {

using TargetType_t = uint32_t;

namespace fs = std::experimental::filesystem;

} //namespace common
#endif // TYPES_H

