#ifndef FILE_NAMES_H
#define FILE_NAMES_H

namespace common {

constexpr auto kDefaultFileSize = 1024ULL * 1024ULL * 1024ULL;

constexpr auto kOrderedFileName = "ordered.bin";
constexpr auto kReverseOrderFileName = "reversedorder.bin";
constexpr auto kBigSawToothWaveFileName = "bigsawtoothwaves.bin";
constexpr auto kReversedBigSawToothWaveFileName = "reversedbigsawtoothwaves.bin";
constexpr auto kSmallSawToothWaveFileName = "smallsawtoothwaves.bin";
constexpr auto kReversedSmallSawToothWaveFileName = "reversedsmallsawtoothwaves.bin";

constexpr auto kBigSawToothWavesCount  = 8;     // Total waves count for saw tooth files. Wave size = file size / waves count
constexpr auto kSmallSawToothWavesSize = 1024; // Each wave size for saw tooth files. Waves count = file size / waves size

} // namespace common
#endif // FILE_NAMES_H

