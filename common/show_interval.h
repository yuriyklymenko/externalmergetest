#ifndef SHOW_INTERVAL_H
#define SHOW_INTERVAL_H

#include <chrono>
#include <iostream>

namespace common {

class ShowInterval
{
    using time_interval_t = std::chrono::microseconds;
    using clock = std::chrono::high_resolution_clock;

private:
    const std::chrono::time_point<clock> start_ = clock::now();
    const std::string name_;

public:
    ShowInterval(std::string name)
        : start_(clock::now())
        ,  name_(name)
    {}

    ~ShowInterval() {
        auto diff = std::chrono::duration_cast<time_interval_t>(clock::now() - start_);
        std::cout << name_ << ": " << diff.count() << "us" << std::endl;
    }
};

} // namespace common
#endif // SHOW_INTERVAL_H

