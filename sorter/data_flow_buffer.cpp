#include "data_flow_buffer.h"

namespace sorter
{

DataFlowBuffer::DataFlowBuffer(int buffer_size)
    : data_buffer_(new TargetType_t[buffer_size])
    , data_index_(0)
    , buffer_size_(buffer_size)
    , size_(0)
    , empty_(false)
    , thread_stop_flag_(false)
    , thread_doload_flag_(false)
    , thread_loaded_flag_(false)
    , thread_loaded_count_(0)
    , thread_(std::thread(&DataFlowBuffer::thread_function, this))
{
}

DataFlowBuffer::~DataFlowBuffer()
{
    stopThread();
    delete[] data_buffer_;
}

void DataFlowBuffer::thread_function()
{
    while(true)
    {
        // Wait until we need to load new data or exit
        {
            std::unique_lock<std::mutex> lk(thread_states_locker_);
            thread_stopper_.wait(lk, [&](){return thread_doload_flag_ || thread_stop_flag_; });
        }

        if (thread_stop_flag_)
        {
            return;
        }

        thread_doload_flag_ = false;
        // Call the loading data implementation which is supposed to be implemented by inherited classes
        size_ = loadNewDataImpl();
        data_index_ = 0;
        empty_ = size_==0;

        // Loading is done
        {
            std::unique_lock<std::mutex> lk(thread_states_locker_);
            thread_loaded_flag_ = true;
            thread_waiter_.notify_one();
        }
    }
}

void DataFlowBuffer::stopThread()
{
    {
        std::unique_lock<std::mutex> lk(thread_states_locker_);
        thread_stop_flag_ = true;
        thread_stopper_.notify_one();
    }
    thread_.join();
}

void DataFlowBuffer::loadNewData()
{
    if (!empty_)
    {
        std::unique_lock<std::mutex> lk(thread_states_locker_);
        thread_doload_flag_ = true;
        thread_stopper_.notify_one();
    }
}

bool DataFlowBuffer::waitForNewData()
{
    if (!empty_)
    {
        std::unique_lock<std::mutex> lk(thread_states_locker_);
        thread_waiter_.wait(lk, [&](){return thread_loaded_flag_ || thread_stop_flag_; });
        thread_loaded_flag_= false;
    }
    return !empty_;
}


} //namespace sorter
