#include "input_file_buffer.h"

#include <iostream>

namespace sorter
{

InputFileBuffer::InputFileBuffer(const fs::path file_path, int size)
    : DataFlowBuffer(size)
    , file_path_(file_path)
{
    file_.open(file_path, std::ios::in | std::ios::binary);

    if (!file_.good())
    {
        std::cerr << "Coudn't open file " << file_path_.c_str() << " for reading!" << std::endl;
        std::abort();
    }
}

InputFileBuffer::~InputFileBuffer()
{
    file_.close();
}

int InputFileBuffer::loadNewDataImpl()
{
    int loaded = 0;

    if (file_.good())
    {
        file_.read(reinterpret_cast<char*>(getBuffer()), getBufferSize()*sizeof(TargetType_t));
        auto read_count = file_.gcount();

        if ((read_count % sizeof(TargetType_t)) > 0)
        {
            std::cerr << "File " << file_path_.c_str() << " size looks inconsistently!" << std::endl;
            std::abort();
        }

        loaded = read_count / sizeof(TargetType_t);
    }

    return loaded;
}

} //namespace sorter

