#include "heap_sorting.h"

namespace sorter
{

// Implementation of Heap Sorting

static void sortShiftDown(TargetType_t* block, int root, int bottom)
{
    int maxChild; // index of Max
    auto rootx2 = root*2;

    while (rootx2 <= bottom)
    {
        if (rootx2 == bottom)
        {
            maxChild = rootx2;
        }
        else if (block[rootx2] > block[rootx2 + 1])
        {
            maxChild = rootx2;
        }
        else
        {
            maxChild = rootx2 + 1;
        }

        if (block[root] < block[maxChild])
        {
            auto temp = block[root];
            block[root] = block[maxChild];
            block[maxChild] = temp;
            root = maxChild;
            rootx2 = root*2;
        }
        else
        {
            return;
        }
    }
}

void heapSorting(TargetType_t* block, int block_size)
{
    // Do bottome line of heap
    for (int i = (block_size / 2) - 1; i >= 0; --i)
    {
        sortShiftDown(block, i, block_size - 1);
    }
    // Do shift down fo the rest of heap
    for (int i = block_size - 1; i >= 1; --i)
    {
        auto temp = block[0];
        block[0] = block[i];
        block[i] = temp;
        sortShiftDown(block, 0, i - 1);
    }
}

} //namespace sorter

