#ifndef SORTER_EXTERNAL_SORTING_H
#define SORTER_EXTERNAL_SORTING_H

#include <list>
#include <mutex>
#include <condition_variable>

#include "types.h"
#include "output_file_buffer.h"

namespace sorter
{

class ExternalSorting
{
public:
    ExternalSorting(BlockSortingStrategy sorting_method) : sorting_method_(sorting_method) {}
    virtual ~ExternalSorting();

    void doSort(SortingOptions& options);

private:
    BlockSortingStrategy sorting_method_;

    void doSplittingPhase(SortingOptions& options);
    void doMergingPhase(SortingOptions& options);

    std::list<fs::path> block_chain_files_;
    int last_chain_index_;
    fs::path getUniqueChainFilePath(const fs::path& output_file_path);
    void removeChainFiles();

    std::list<TargetType_t*> free_splitting_blocks_;
    std::mutex free_blocks_locker_;
    std::condition_variable free_blocks_condition_;

    void initSplittingBlocksStorage(int count, int size);
    TargetType_t* getSplittingBlock();
    void returnSplittingBlock(TargetType_t* block);
    void freeSplittingBlocksStorage();

    std::mutex sequnetial_writing_locker_;
    void sortSplittedBlock(TargetType_t* block, int block_size, const fs::path& output_file);

    OutputFileBufferPtr buildMergingTree(const SortingOptions& options);
    uint getMergingBuffersCount(uint merge_in_N_way) const;
};

} //namespace sorter
#endif // SORTER_EXTERNAL_SORTING_H
