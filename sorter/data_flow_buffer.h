#ifndef SORTER_DATA_FLOW_BUFFER_H
#define SORTER_DATA_FLOW_BUFFER_H

#include <tuple>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "types.h"

namespace sorter
{

class DataFlowBuffer
{
public:

    DataFlowBuffer(int buffer_size);
    virtual ~DataFlowBuffer();

    /// Shifts data pointer by one element and return true if need to be followed with waitForNewData()
    /// Those functions are separated to make available do a few loads simuntaniously!
    inline bool advanceAndRequestForNewData()
    {
        if (++data_index_ >= size_)
        {
            loadNewData();
            return true;
        }

        return false;
    }

    /// Waits until background process load new portion of data
    /// Need to ba called if advanceAndRequestForNewData() returned true
    bool waitForNewData();

    /// Returns pointed data and true of just false if there no more data
    inline const TargetType_t& getValue() const
    {
        return data_buffer_[data_index_];
    }

    bool isEmpty() {return empty_;}

    std::thread::id getThreadId() { return thread_.get_id(); }

protected:
    TargetType_t* getBuffer() const {return data_buffer_;}
    void updateSize(int size) {size_ = size;}
    int getBufferSize() const {return buffer_size_;}

    /// Load new portion of data implementation and return loaded size
    virtual int loadNewDataImpl() = 0;

private:
    TargetType_t* data_buffer_;
    int data_index_;
    const int buffer_size_;
    int size_;
    bool empty_;

    bool thread_stop_flag_;
    bool thread_doload_flag_;
    bool thread_loaded_flag_;
    int  thread_loaded_count_;
    std::mutex thread_states_locker_;
    std::condition_variable thread_stopper_;
    std::condition_variable thread_waiter_;

    std::thread thread_;
    void thread_function();
    void stopThread();

    void loadNewData();
};
using DataFlowBufferPtr = std::shared_ptr<DataFlowBuffer>;

} //namespace sorter
#endif // SORTER_DATA_FLOW_BUFFER_H

