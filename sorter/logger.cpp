#include "logger.h"

#include <iostream>
#include <mutex>


namespace sorter
{

static std::mutex log_locker;

bool isLogEnabled() { return true; }

void log(const std::string& position, const std::string& message)
{
    log_locker.lock();
    std::cout << position << " : " << message << std::endl;
    log_locker.unlock();
}

} //namespace soerter
