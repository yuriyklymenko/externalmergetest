#include <iostream>
#include <fstream>
#include <thread>
#include <tuple>

#include "../common/types.h"
#include "../common/show_interval.h"

#include "configuration.h"
#include "external_sorting.h"
#include "heap_sorting.h"

namespace sorter
{

std::tuple<fs::path, fs::path> parseCommandLineOptions(int argc, char** argv)
{
    if (argc != 3)
    {
        throw std::invalid_argument("two command line options are expected");
    }

    return std::make_tuple(fs::path(argv[1]), fs::path(argv[2]));
}

void updateDefaultParameters(SortingOptions& options)
{
    options.memory_usage_limit = kMaxUsedMemorySize;
    options.threads_count = std::thread::hardware_concurrency() ? std::thread::hardware_concurrency() : kDefaultThreadsCount;
    options.merge_in_N_way = kMegeInNWay;
}


} //namespace sorter

using namespace sorter;
using namespace common;

int main(int argc, char** argv)
{
    SortingOptions options;

    try
    {
        fs::path input_file_path;

        std::tie(input_file_path, options.output_file_path) = parseCommandLineOptions(argc, argv);
        updateDefaultParameters(options);

        options.input_file.open(input_file_path, std::ios::in | std::ios::binary);
        if (!options.input_file.good())
        {
            throw std::invalid_argument(std::string("the file ") + input_file_path.c_str() + " coudn't be opened for reading!");
        }

        // Open output file on the start to avoid any issues on the lates stages and to partialy check that we have rights for writing to the directory
        options.output_file.open(options.output_file_path, std::ios::out | std::ios::binary);
        if (!options.output_file.good())
        {
            throw std::invalid_argument(std::string("the file ") + input_file_path.c_str() + " coudn't be opened for writing!");
        }

    }
    catch (std::exception& ex)
    {
        std::cout << "Failed parsing arguments: " << ex.what() << std::endl;
        std::cout << "Please use: " << fs::path(argv[0]).filename().c_str() << " <input file name> <output file name>" << std::endl;
        return -1;
    }

    try
    {
        ExternalSorting sorter(&heapSorting);
        sorter.doSort(options);
    }
    catch (std::exception& ex)
    {
        std::cerr << "Sorting failed: " << ex.what() << std::endl;
        return -1;
    }

    options.input_file.close();
    options.output_file.close();

    return 0;
}


