#ifndef SORTER_LOGGER_H
#define SORTER_LOGGER_H

#include <sstream>
#include <string.h>

namespace sorter
{

bool isLogEnabled();
void log(const std::string& position, const std::string& message);

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define LOG(args) \
     if (isLogEnabled()) \
     { \
         std::stringstream position; \
         position << __FILENAME__ << ":" << __LINE__; \
         std::stringstream _log_message; \
         _log_message << args; \
         log(position.str(), _log_message.str()); \
     }
} // namespace sorter
#endif // SORTER_LOGGER_H
