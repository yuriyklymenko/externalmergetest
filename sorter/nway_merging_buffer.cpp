#include "nway_merging_buffer.h"

#include <iostream>

namespace sorter
{

NWayMergingBuffer::NWayMergingBuffer(const std::vector<DataFlowBufferPtr>& ways_ptrs, int buffer_size)
    : DataFlowBuffer(buffer_size)
{
    ways_.reserve(ways_ptrs.size());
    for (auto& ways_ptr: ways_ptrs)
    {
        ways_.emplace_back(Way(ways_ptr, false, false));
    }
}

NWayMergingBuffer::~NWayMergingBuffer()
{
}

int NWayMergingBuffer::loadNewDataImpl()
{
    int loaded = 0;

    if (!isEmpty() && (ways_.size() > 0))
    {
        for (auto& way: ways_)
        {
            if(!way.advanced)
            {
                way.need_to_wait = way.way->advanceAndRequestForNewData();
                way.advanced = true;
            }
        }

        TargetType_t* buffer = getBuffer();

        for (int i=0 ; i < getBufferSize() ; ++i)
        {
            Ways::iterator min_it = ways_.end();
            TargetType_t min_value;
            bool min_is_set = false;
            for (Ways::iterator it=ways_.begin(); it != ways_.end(); /*it increment managed in the loop*/)
            {
                bool this_way_is_empty = false;
                if (it->need_to_wait)
                {
                    if (it->way->waitForNewData())
                    {
                        it->need_to_wait = false;
                    }
                    else
                    {
                        this_way_is_empty = true;
                    }
                }

                if (this_way_is_empty)
                {
                    it = ways_.erase(it);
                }
                else
                {
                    if (!min_is_set)
                    {
                        min_it = it;
                        min_value = min_it->way->getValue();
                        min_is_set = true;
                    }
                    else
                    {
                        if (min_value > it->way->getValue())
                        {
                            min_value = it->way->getValue();
                            min_it = it;
                        }
                    }
                    it++;
                }
            }
            if (min_is_set)
            {
                buffer[loaded++] = min_value;
                min_it->need_to_wait = min_it->way->advanceAndRequestForNewData();
            }
        }
    }
    return loaded;
}

} //namespace sorter
