#ifndef SORTER_INPUT_FILE_BUFFER_H
#define SORTER_INPUT_FILE_BUFFER_H

#include "data_flow_buffer.h"

namespace sorter
{

class InputFileBuffer : public DataFlowBuffer
{
public:
    InputFileBuffer(const fs::path file_path, int size);
    virtual ~InputFileBuffer();

protected:
    // Implementation of DataFlowBuffer
    int loadNewDataImpl() override;

private:
    fs::path file_path_;
    std::ifstream file_;
};

} //sorter
#endif // SORTER_INPUT_FILE_DATA_FLOW_H
