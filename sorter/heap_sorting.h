#ifndef SORTER_HEAP_SORTING_H
#define SORTER_HEAP_SORTING_H

#include "types.h"

namespace sorter
{

void heapSorting(TargetType_t* block, int block_size);

} //namespace sorter
#endif // SORTER_HEAP_SORTING_H
