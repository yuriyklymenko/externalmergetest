#ifndef SORTER_TYPES_H
#define SORTER_TYPES_H

#include <fstream>
#include <thread>
#include <iostream>

#include "../common/types.h"

using namespace common;

namespace sorter
{

struct SortingOptions
{
    std::ifstream input_file;

    fs::path output_file_path;
    std::ofstream output_file;

    uint memory_usage_limit;
    uint threads_count;
    uint merge_in_N_way;
};

using BlockSortingStrategy = std::function<void(TargetType_t* block, int block_size)>;

} // namespace sorter
#endif // SORTER_TYPES_H

