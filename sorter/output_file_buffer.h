#ifndef SORTER_OUTPUT_FILE_BUFFER_H
#define SORTER_OUTPUT_FILE_BUFFER_H

#include <fstream>

#include "nway_merging_buffer.h"

namespace sorter
{

class OutputFileBuffer : public NWayMergingBuffer
{
public:
    OutputFileBuffer(const std::vector<DataFlowBufferPtr>& ways_ptrs, int buffer_size);
    virtual ~OutputFileBuffer() {}

    void mergeToFile(std::ofstream& file);
};
using OutputFileBufferPtr = std::shared_ptr<OutputFileBuffer>;

} //namespace sorter
#endif // SORTER_OUTPUT_FILE_BUFFER_H
