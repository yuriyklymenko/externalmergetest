#ifndef SORTER_CONFIGURATION_H
#define SORTER_CONFIGURATION_H

#include <stdint.h>

namespace sorter
{

/// Max amount of memory used for blocks under sorting, loading or storing
constexpr auto kMaxUsedMemorySize = 256 * 1024 * 1024;

/// Total count of paralel computing blocks in first phase
constexpr auto kDefaultThreadsCount = 4;

/// N-Way paralleling parameter (count of subbrunches in each N-Way merger)
constexpr auto kMegeInNWay = 4;


} //namespace sorter
#endif // SORTER_CONFIGURATION_H

