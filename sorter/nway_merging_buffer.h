#ifndef SORTER_N_WAY_MERGING_BUFFER_H
#define SORTER_N_WAY_MERGING_BUFFER_H

#include "data_flow_buffer.h"

namespace sorter
{

class NWayMergingBuffer : public DataFlowBuffer
{
public:
    NWayMergingBuffer(const std::vector<DataFlowBufferPtr>& ways_ptrs, int buffer_size);
    virtual ~NWayMergingBuffer();

protected:
    // Implementation of DataFlowBuffer
    int loadNewDataImpl() override;

private:
    struct Way{
        Way(DataFlowBufferPtr way_ptr, bool advanced_flag, bool need_to_wait_flag)
            : way(way_ptr)
            , advanced(advanced_flag)
            , need_to_wait(need_to_wait_flag)
            {}
        DataFlowBufferPtr way;
        bool advanced;
        bool need_to_wait;
    };

    using Ways = std::vector<Way>;
    Ways ways_;
};

} //namespace sorter
#endif // SORTER_TWO_WAY_MERGING_BUFFER_H
