#include "output_file_buffer.h"

#include <iostream>

namespace sorter
{

OutputFileBuffer::OutputFileBuffer(const std::vector<DataFlowBufferPtr>& ways_ptrs, int buffer_size)
    : NWayMergingBuffer(ways_ptrs, buffer_size)
{}

void OutputFileBuffer::mergeToFile(std::ofstream& file)
{
    while(int loaded = loadNewDataImpl())
    {
        file.write(reinterpret_cast<char*>(getBuffer()), loaded * sizeof(TargetType_t));
    }
}


} //namespace sorter
