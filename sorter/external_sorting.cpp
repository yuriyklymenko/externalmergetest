#include "external_sorting.h"

#include <sstream>
#include <iomanip>

#include "../common/show_interval.h"

#include "data_flow_buffer.h"
#include "output_file_buffer.h"
#include "input_file_buffer.h"

namespace sorter
{

ExternalSorting::~ExternalSorting()
{
    freeSplittingBlocksStorage();
    removeChainFiles();
}

void ExternalSorting::doSort(SortingOptions& options)
{
    ShowInterval timer("Total sorting time");

    doSplittingPhase(options);
    doMergingPhase(options);
}

void ExternalSorting::initSplittingBlocksStorage(int count, int size)
{
    std::unique_lock<std::mutex> lk(free_blocks_locker_);
    for (int i=0; i < count ; ++i)
    {
        free_splitting_blocks_.emplace_back(new TargetType_t[size]);
    }
}

TargetType_t* ExternalSorting::getSplittingBlock()
{
    std::unique_lock<std::mutex> lk(free_blocks_locker_);
    free_blocks_condition_.wait(lk, [&](){return free_splitting_blocks_.size() > 0; });

    TargetType_t* block = free_splitting_blocks_.front();
    free_splitting_blocks_.pop_front();
    return block;
}

void ExternalSorting::returnSplittingBlock(TargetType_t* block)
{
    std::unique_lock<std::mutex> lk(free_blocks_locker_);
    free_splitting_blocks_.emplace_back(block);
    free_blocks_condition_.notify_one();
}

void ExternalSorting::freeSplittingBlocksStorage()
{
    std::unique_lock<std::mutex> lk(free_blocks_locker_);
    for (auto& block: free_splitting_blocks_)
    {
        delete[] block;
    }
    free_splitting_blocks_.clear();
}

fs::path ExternalSorting::getUniqueChainFilePath(const fs::path& output_file_path)
{
    while(true)
    {
        fs::path new_chain_file = output_file_path;
        std::stringstream indexed_extension;
        indexed_extension << std::setw(3) << std::setfill('0') << last_chain_index_++;
        new_chain_file.replace_extension(indexed_extension.str());
        if (!fs::is_regular_file(new_chain_file))
        {
            return new_chain_file;
        }
    }
}

void ExternalSorting::removeChainFiles()
{
    for (auto& file_path: block_chain_files_)
    {
        fs::remove(file_path);
    }
    block_chain_files_.clear();
}

void ExternalSorting::doSplittingPhase(SortingOptions& options)
{
    ShowInterval timer("Splitting time");

    auto sorting_block_size = options.memory_usage_limit / options.threads_count / sizeof(TargetType_t);
    initSplittingBlocksStorage(options.threads_count, sorting_block_size);
    last_chain_index_ = 0;

    std::list<std::thread> started_threads;

    while(options.input_file.good())
    {
        TargetType_t* block = getSplittingBlock();
        options.input_file.read(reinterpret_cast<char*>(block), sorting_block_size*sizeof(TargetType_t));
        auto read_count = options.input_file.gcount();

        if (read_count)
        {
            if (read_count % sizeof(TargetType_t))
            {
                std::cout << "File size is not multiplied by " << sizeof(TargetType_t) << " detected. The rest of file will be ignored!" << std::endl;
            }
            auto block_size = read_count / sizeof(TargetType_t);
            block_chain_files_.emplace_back(getUniqueChainFilePath(options.output_file_path));
            started_threads.emplace_back(std::thread(&ExternalSorting::sortSplittedBlock, this, block, block_size, block_chain_files_.back()));

            while (started_threads.size() > options.threads_count)
            {
                started_threads.front().detach();
                started_threads.pop_front();
            }
        }
    }

    for (auto& thread: started_threads)
    {
        thread.join();
    }

    freeSplittingBlocksStorage();
}

void ExternalSorting::sortSplittedBlock(TargetType_t* block, int block_size, const fs::path& output_file)
{
    sorting_method_(block, block_size);
    {
        std::lock_guard<std::mutex> lk(sequnetial_writing_locker_);
        std::ofstream file;
        file.open(output_file, std::ios::out | std::ios::binary);
        file.write(reinterpret_cast<char*>(block), block_size * sizeof(TargetType_t));
        file.close();
    }
    returnSplittingBlock(block);
}

void ExternalSorting::doMergingPhase(SortingOptions& options)
{
    ShowInterval timer("Merging time");

    OutputFileBufferPtr out_file_merger = buildMergingTree(options);
    out_file_merger->mergeToFile(options.output_file);

    removeChainFiles();
}

uint ExternalSorting::getMergingBuffersCount(uint merge_in_N_way) const
{
    uint total_buffers_count = block_chain_files_.size();
    uint level_count = block_chain_files_.size();
    while (level_count > merge_in_N_way)
    {
        int move_to_next_level = level_count % merge_in_N_way;
        level_count = level_count/merge_in_N_way + move_to_next_level;
        total_buffers_count += level_count;
    }
    return total_buffers_count + 1;
}


OutputFileBufferPtr ExternalSorting::buildMergingTree(const SortingOptions& options)
{
    auto buffer_size = options.memory_usage_limit / getMergingBuffersCount(options.merge_in_N_way);

    std::vector<DataFlowBufferPtr> bottom_level;
    bottom_level.reserve(block_chain_files_.size());

    // Most bottom level are file readers
    for (auto& file_path: block_chain_files_)
    {
        bottom_level.emplace_back(std::make_shared<InputFileBuffer>(file_path, buffer_size));
    }

    // Next levels are mergers
    while (bottom_level.size() > options.merge_in_N_way)
    {
        std::vector<DataFlowBufferPtr> top_level;
        top_level.reserve((bottom_level.size() / options.merge_in_N_way + options.merge_in_N_way-1));

        std::vector<DataFlowBufferPtr> n_buffers;
        n_buffers.reserve(options.merge_in_N_way);
        for (auto& buffer : bottom_level)
        {
            n_buffers.push_back(buffer);
            if (n_buffers.size() >= options.merge_in_N_way)
            {
                top_level.push_back(std::static_pointer_cast<DataFlowBuffer>(std::make_shared<NWayMergingBuffer>(n_buffers, buffer_size)));
                n_buffers.clear();
                n_buffers.reserve(options.merge_in_N_way);
            }
        }
        top_level.insert(top_level.end(), n_buffers.begin(), n_buffers.end());
        bottom_level = std::move(top_level);
    }

    // Most top level is file saver
    return std::make_shared<OutputFileBuffer>(bottom_level, buffer_size);
}

} //namespace sorter
