#include <iostream>
#include <exception>
#include <fstream>

#include "../common/generator_parameters.h"
#include "../common/types.h"
#include "../common/show_interval.h"

using namespace common;

namespace generator
{

struct Options
{
    uint64_t files_size = kDefaultFileSize;
    fs::path out_dir = fs::current_path();
};

void parseCommandLineOptions(int argc, char** argv, Options& options)
{
    if (argc == 2)
    {
        fs::path out_dir_option = argv[1];
        options.out_dir = fs::canonical(out_dir_option, options.out_dir);

        if (!fs::is_directory(options.out_dir))
        {
            throw std::invalid_argument(std::string("The directory ") + options.out_dir.c_str() + " is not exist!");
        }
    }
    else if (argc > 2)
    {
        throw std::invalid_argument("Unexpected set of arguments!");
    }
}

void generateOrderedFile(const Options& options)
{
    ShowInterval timer(__FUNCTION__);

    fs::path file_path = options.out_dir / kOrderedFileName;
    uint64_t total_counter = options.files_size / sizeof(TargetType_t);
    uint64_t file_size = total_counter * sizeof(TargetType_t);

    std::cout << std::endl
              << "Generating already ordered file: " << std::endl
              << "    path : " << file_path.c_str() << std::endl
              << "    size : " << file_size << std::endl;

    std::ofstream file(file_path, std::ios::out | std::ios::binary);
    TargetType_t val=0;
    for (uint64_t counter=0 ; counter < total_counter ; ++counter)
    {
        file.write(reinterpret_cast<const char*>(&val), sizeof(TargetType_t));
        val++;
    }
    file.close();
}

void generateReverseOrderFile(const Options& options)
{
    ShowInterval timer(__FUNCTION__);

    fs::path file_path = options.out_dir / kReverseOrderFileName;
    uint64_t total_counter = options.files_size / sizeof(TargetType_t);
    uint64_t file_size = total_counter * sizeof(TargetType_t);

    std::cout << std::endl
              << "Generating reverse order file: " << std::endl
              << "    path : " << file_path.c_str() << std::endl
              << "    size : " << file_size << std::endl;

    std::ofstream file(file_path, std::ios::out | std::ios::binary);
    TargetType_t val=total_counter-1;
    for (uint64_t counter=0 ; counter < total_counter ; ++counter)
    {
        file.write(reinterpret_cast<const char*>(&val), sizeof(TargetType_t));
        val--;
    }
    file.close();
}

void generateBigSawToothWaveFile(const Options& options)
{
    ShowInterval timer(__FUNCTION__);

    fs::path file_path = options.out_dir / kBigSawToothWaveFileName;
    uint64_t max_values_counter = options.files_size / sizeof(TargetType_t);
    uint64_t one_wave_size = max_values_counter / kBigSawToothWavesCount;
    uint64_t file_size   = one_wave_size * kBigSawToothWavesCount * sizeof(TargetType_t);

    std::cout << std::endl
              << "Generating big saw tooth waves file: " << std::endl
              << "    path : " << file_path.c_str() << std::endl
              << "    size : " << file_size << std::endl;

    std::ofstream file(file_path, std::ios::out | std::ios::binary);
    for (int wave=0; wave < kBigSawToothWavesCount; ++ wave)
    {
        for (TargetType_t val=1 ; val <=one_wave_size ; ++val)
        {
            file.write(reinterpret_cast<const char*>(&val), sizeof(TargetType_t));
        }
    }
    file.close();
}

void generateReversedBigSawToothWaveFile(const Options& options)
{
    ShowInterval timer(__FUNCTION__);

    fs::path file_path = options.out_dir / kReversedBigSawToothWaveFileName;
    uint64_t max_values_counter = options.files_size / sizeof(TargetType_t);
    uint64_t one_wave_size = max_values_counter / kBigSawToothWavesCount;
    uint64_t file_size   = one_wave_size * kBigSawToothWavesCount * sizeof(TargetType_t);

    std::cout << std::endl
              << "Generating reversed big saw tooth waves file: " << std::endl
              << "    path : " << file_path.c_str() << std::endl
              << "    size : " << file_size << std::endl;

    std::ofstream file(file_path, std::ios::out | std::ios::binary);
    for (int wave=0; wave < kBigSawToothWavesCount; ++ wave)
    {
        for (TargetType_t val=one_wave_size ; val > 0 ; --val)
        {
            file.write(reinterpret_cast<const char*>(&val), sizeof(TargetType_t));
        }
    }
    file.close();
}

void generateSmallSawToothWaveFile(const Options& options)
{
    ShowInterval timer(__FUNCTION__);

    fs::path file_path = options.out_dir / kSmallSawToothWaveFileName;

    uint64_t max_values_counter = options.files_size / sizeof(TargetType_t);
    uint64_t waves_count = max_values_counter / kSmallSawToothWavesSize;
    uint64_t file_size   = waves_count * kSmallSawToothWavesSize * sizeof(TargetType_t);

    std::cout << std::endl
              << "Generating small saw tooth waves file: " << std::endl
              << "    path : " << file_path.c_str() << std::endl
              << "    size : " << file_size << std::endl;

    std::ofstream file(file_path, std::ios::out | std::ios::binary);
    for (int wave=0; wave < waves_count; ++ wave)
    {
        for (TargetType_t val=1 ; val <=kSmallSawToothWavesSize ; ++val)
        {
            file.write(reinterpret_cast<const char*>(&val), sizeof(TargetType_t));
        }
    }
    file.close();
}

void generateReversedSmallSawToothWaveFile(const Options& options)
{
    ShowInterval timer(__FUNCTION__);

    fs::path file_path = options.out_dir / kReversedSmallSawToothWaveFileName;

    uint64_t max_values_counter = options.files_size / sizeof(TargetType_t);
    uint64_t waves_count = max_values_counter / kSmallSawToothWavesSize;
    uint64_t file_size   = waves_count * kSmallSawToothWavesSize * sizeof(TargetType_t);

    std::cout << std::endl
              << "Generating reversed small saw tooth waves file: " << std::endl
              << "    path : " << file_path.c_str() << std::endl
              << "    size : " << file_size << std::endl;

    std::ofstream file(file_path, std::ios::out | std::ios::binary);
    for (int wave=0; wave < waves_count; ++ wave)
    {
        for (TargetType_t val=kSmallSawToothWavesSize ; val > 0 ; --val)
        {
            file.write(reinterpret_cast<const char*>(&val), sizeof(TargetType_t));
        }
    }
    file.close();
}

} //namespace generator

using namespace generator;

int main(int argc, char** argv)
{
    Options options;

    try {
        parseCommandLineOptions(argc, argv, options);
    }
    catch (std::exception& ex)
    {
        std::cout << "Failed parsing arguments: " << ex.what() << std::endl;
        std::cout << "Please use: " << fs::path(argv[0]).filename().c_str() << " <optional output dir>" << std::endl;
        return -1;
    }

    generateOrderedFile(options);
    generateReverseOrderFile(options);
    generateBigSawToothWaveFile(options);
    generateReversedBigSawToothWaveFile(options);
    generateSmallSawToothWaveFile(options);
    generateReversedSmallSawToothWaveFile(options);

    return 0;
}


