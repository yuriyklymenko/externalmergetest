#include <iostream>
#include <exception>
#include <fstream>
#include <stdint.h>
#include <assert.h>

#include "../common/generator_parameters.h"
#include "../common/types.h"
#include "../common/show_interval.h"

using namespace common;

namespace checker
{

struct Options
{
    uint64_t files_size = kDefaultFileSize;
    fs::path input_dir = fs::current_path();
};

struct Statistics
{
    uint64_t values;
    TargetType_t min;
    TargetType_t max;
    uint64_t shortest_sequence;
    uint64_t longest_sequence;
};

void parseCommandLineOptions(int argc, char** argv, Options& options)
{
    if (argc == 2)
    {
        fs::path in_dir_option = argv[1];
        options.input_dir = fs::canonical(in_dir_option, options.input_dir);

        if (!fs::is_directory(options.input_dir))
        {
            throw std::invalid_argument(std::string("The directory ") + options.input_dir.c_str() + " is not exist!");
        }
    }
    else if (argc > 2)
    {
        throw std::invalid_argument("Unexpected set of arguments!");
    }
}

void checkSequnceStatistics(Statistics& stat, uint64_t seq)
{
    if (stat.shortest_sequence > seq) stat.shortest_sequence = seq;
    if (stat.longest_sequence < seq) stat.longest_sequence = seq;
}

Statistics getFileStatistics(fs::path file_path)
{
    Statistics stat = {0};
    stat.shortest_sequence = UINT64_MAX;
    stat.longest_sequence  = 0;

    std::ifstream file(file_path, std::ios::in | std::ios::binary);
    bool not_first_value = false;
    uint64_t seq=0;

    while(true)
    {
        TargetType_t value;
        TargetType_t prev_value;
        file.read(reinterpret_cast<char*>(&value), sizeof(TargetType_t));
        if (file.good())
        {
            stat.values++;
            if (not_first_value)
            {
                if (stat.min > value) stat.min = value;
                if (stat.max < value) stat.max = value;

                assert(prev_value <= value);

                if (prev_value != value)
                {
                    checkSequnceStatistics(stat, seq);
                    seq=0;
                }
            }
            else
            {
                stat.min = stat.max = value;
                not_first_value = true;
            }
            prev_value = value;
            seq++;
        }
        else
        {
            break;
        }
    }
    checkSequnceStatistics(stat, seq);
    file.close();
    return stat;
}

void checkOrderedFile(const Options& options)
{
    std::cout << "Checking already ordered file ..." << std::endl;

    fs::path file_path = options.input_dir / kOrderedFileName;
    Statistics stat = getFileStatistics(file_path);

    uint64_t total_counter = options.files_size / sizeof(TargetType_t);

    assert(stat.values == total_counter);
    assert(stat.min == 0);
    assert(stat.max == total_counter-1);
    assert(stat.longest_sequence == 1);
    assert(stat.shortest_sequence == 1);
}

void checkReversedOrderedFile(const Options& options)
{
    std::cout << "Checking reverse ordered file ..." << std::endl;

    fs::path file_path = options.input_dir / kReverseOrderFileName;
    Statistics stat = getFileStatistics(file_path);

    uint64_t total_counter = options.files_size / sizeof(TargetType_t);

    assert(stat.values == total_counter);
    assert(stat.min == 0);
    assert(stat.max == total_counter-1);
    assert(stat.longest_sequence == 1);
    assert(stat.shortest_sequence == 1);
}

void checkBigSawToothWaveFile(const Options& options)
{
    std::cout << "Checking big saw tooth waves file ..." << std::endl;

    fs::path file_path = options.input_dir / kBigSawToothWaveFileName;
    Statistics stat = getFileStatistics(file_path);

    uint64_t max_values_counter = options.files_size / sizeof(TargetType_t);
    uint64_t one_wave_size = max_values_counter / kBigSawToothWavesCount;
    uint64_t total_counter = one_wave_size * kBigSawToothWavesCount;

    assert(stat.values == total_counter);
    assert(stat.min == 1);
    assert(stat.max == one_wave_size);
    assert(stat.longest_sequence == kBigSawToothWavesCount);
    assert(stat.shortest_sequence == kBigSawToothWavesCount);
}

void checkReversedBigSawToothWaveFile(const Options& options)
{
    std::cout << "Checking reversed big saw tooth waves file ..." << std::endl;

    fs::path file_path = options.input_dir / kReversedBigSawToothWaveFileName;
    Statistics stat = getFileStatistics(file_path);

    uint64_t max_values_counter = options.files_size / sizeof(TargetType_t);
    uint64_t one_wave_size = max_values_counter / kBigSawToothWavesCount;
    uint64_t total_counter = one_wave_size * kBigSawToothWavesCount;

    assert(stat.values == total_counter);
    assert(stat.min == 1);
    assert(stat.max == one_wave_size);
    assert(stat.longest_sequence == kBigSawToothWavesCount);
    assert(stat.shortest_sequence == kBigSawToothWavesCount);
}

void checkSmallSawToothWaveFile(const Options& options)
{
    std::cout << "Checking small saw tooth waves file ..." << std::endl;

    fs::path file_path = options.input_dir / kSmallSawToothWaveFileName;
    Statistics stat = getFileStatistics(file_path);

    uint64_t max_values_counter = options.files_size / sizeof(TargetType_t);
    uint64_t waves_count = max_values_counter / kSmallSawToothWavesSize;
    uint64_t total_counter = waves_count * kSmallSawToothWavesSize;

    assert(stat.values == total_counter);
    assert(stat.min == 1);
    assert(stat.max == kSmallSawToothWavesSize);
    assert(stat.longest_sequence == waves_count);
    assert(stat.shortest_sequence == waves_count);
}

void checkReversedSmallSawToothWaveFile(const Options& options)
{
    std::cout << "Checking reversed small saw tooth waves file ..." << std::endl;

    fs::path file_path = options.input_dir / kReversedSmallSawToothWaveFileName;
    Statistics stat = getFileStatistics(file_path);

    uint64_t max_values_counter = options.files_size / sizeof(TargetType_t);
    uint64_t waves_count = max_values_counter / kSmallSawToothWavesSize;
    uint64_t total_counter = waves_count * kSmallSawToothWavesSize;

    assert(stat.values == total_counter);
    assert(stat.min == 1);
    assert(stat.max == kSmallSawToothWavesSize);
    assert(stat.longest_sequence == waves_count);
    assert(stat.shortest_sequence == waves_count);
}

} //namespace checker

using namespace checker;

int main(int argc, char** argv)
{
    Options options;

    try {
        parseCommandLineOptions(argc, argv, options);
    }
    catch (std::exception& ex)
    {
        std::cout << "Failed parsing arguments: " << ex.what() << std::endl;
        std::cout << "Please use: " << fs::path(argv[0]).filename().c_str() << " <optional input dir>" << std::endl;
        return -1;
    }

    checkOrderedFile(options);
    checkReversedOrderedFile(options);
    checkBigSawToothWaveFile(options);
    checkReversedBigSawToothWaveFile(options);
    checkSmallSawToothWaveFile(options);
    checkReversedSmallSawToothWaveFile(options);

    std::cout << std::endl << "Done!" << std::endl;
    return 0;
}


